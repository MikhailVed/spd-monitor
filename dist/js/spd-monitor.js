function scrollProjects () {

    var scrollableArray = document.getElementsByClassName('js-scrollable-block');

    var scrollSpeed = 200,
        img,
        contentHeight,
        resizeTimeout;
    


    function getImg (selector) {
        return selector.getElementsByTagName('img')[0];
    }
    
    function setAttributes(el, attrs) {
        for(var key in attrs) {
            el.setAttribute(key, attrs[key]);
        }
    }


    window.addEventListener('resize', function () {

        for (var i = 0; i < scrollableArray.length; i++) {
            img = scrollableArray[i].getElementsByTagName('img')[0];
            img.style.transitionDuration = 0 + 's';
            img.style.marginTop = 0;
        }

            clearTimeout(resizeTimeout);

        resizeTimeout = setTimeout((function () {
            for (var i = 0; i < scrollableArray.length; i++) {

                clearInterval(scrollInterval);

                img = getImg(scrollableArray[i]);
                
                contentHeight = img.clientHeight - img.parentNode.clientHeight;

                setAttributes(img, {'data-animation': 0, 'data-scroll-end': contentHeight});

                img.style.transitionDuration = contentHeight / scrollSpeed + 's';

            }
        }), 100);



    });

    for (var i = 0; i < scrollableArray.length; i++) {
        var scrollInterval;

        img = getImg(scrollableArray[i]);

        contentHeight = img.clientHeight - img.parentNode.clientHeight;

        setAttributes(img, {'data-scroll-end': contentHeight, 'data-animation': 0, 'data-scroll-direction': 'down'});

        img.style.transitionDuration = contentHeight/scrollSpeed + 's';
        
        scrollableArray[i].addEventListener('mouseenter', function () {
            var _self = this,
                thisImg = _self.getElementsByTagName('img')[0],
                thisContentHeight = thisImg.clientHeight - thisImg.parentNode.clientHeight,
                scrollDirection = thisImg.getAttribute('data-scroll-direction');
            
            thisImg.style.transitionDuration = thisContentHeight/scrollSpeed +'s';
            thisImg.style.marginTop = -(thisContentHeight) + 'px';

            scrollInterval = setInterval(function () {

                var stepAnimation = -Math.floor(parseFloat(getComputedStyle(thisImg).getPropertyValue("margin-top")));
                
                if (scrollDirection == 'down') {

                    stepAnimation = -Math.floor(parseFloat(getComputedStyle(thisImg).getPropertyValue("margin-top")));

                    thisImg.style.marginTop =  -(thisContentHeight) + 'px';
                    
                    if(stepAnimation >= thisContentHeight) {
                        scrollDirection = 'up';
                    }

                } else if (scrollDirection == 'up') {

                    thisImg.style.marginTop = 1 + 'px';

                    if (+stepAnimation <= 0) {
                        scrollDirection = 'down';
                        thisImg.style.marginTop = -(thisContentHeight) + 'px';
                    }

                }

                thisImg.setAttribute('data-animation', stepAnimation);

            }, 10);

        });

        scrollableArray[i].addEventListener('mouseleave', function () {
            var _self = this,
                thisImg = _self.getElementsByTagName('img')[0];

            clearInterval(scrollInterval);


            thisImg.style.marginTop = 0;

            thisImg.style.transitionDuration = 0.5 + 's';

            setAttributes(thisImg, {'data-animation': 0, 'data-scroll-direction': 'down'});

        })
    }
}