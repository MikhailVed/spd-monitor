var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    del = require('del');

    gulp.task('sass', function () {
    return gulp.src(['src/sass/**/*.sass', 'src/sass/**/**/*.sass'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({browsers: ['last 15 versions'], cascade: true}))
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'src'
        },
        notify: false
    });
});

gulp.task('clean', function () {
    return del.sync('dist');
});

gulp.task('watch', ['browser-sync', 'sass'], function () {
    gulp.watch(['src/sass/**/*.sass', 'src/sass/**/**/*.sass'], ['sass']);
    gulp.watch('src/*.html', browserSync.reload);
    gulp.watch('src/js/**/*.js', browserSync.reload);
});

gulp.task('build', ['clean', 'sass'], function () {
    gulp.src('src/js/spd-monitor.js')
        .pipe(gulp.dest('dist/js'));

    gulp.src('src/css/spd-monitor.css')
        .pipe(gulp.dest('dist/css'));

    gulp.src('src/img/monitor.svg')
        .pipe(gulp.dest('dist/img'));
});